import bcrypt from 'bcrypt';

export const bcryptImpl = async (password: string) => {
  console.log('BCRYPT:\n');
  console.log(`Password is: ${password}\n`);

  const hash = await bcrypt.hash(password, 10);
  console.log(`bcrypt hash: ${hash}\n`);

  console.log('Comparing password with hash.\n');
  const compare = await bcrypt.compare(password, hash);

  if (compare) {
    console.log('Password matches hash!\n');
  } else {
    console.log("Password doesn't match hash.\n");
  }
};
