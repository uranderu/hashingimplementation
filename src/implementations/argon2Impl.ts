import argon2 from 'argon2';

export const argon2Impl = async (password: string) => {
  console.log('ARGON2:\n');
  console.log(`Password is: ${password}\n`);

  const hash = await argon2.hash(password);
  console.log(`argon2 hash: ${hash}\n`);

  console.log('Comparing password with hash.\n');
  const compare = await argon2.verify(hash, password);

  if (compare) {
    console.log('Password matches hash!\n');
  } else {
    console.log("Password doesn't match hash.\n");
  }
};
