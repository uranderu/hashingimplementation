import { bcryptImpl } from './implementations/bcryptImpl';
import * as readLine from 'readline';
import { argon2Impl } from './implementations/argon2Impl';

const rl = readLine.createInterface(process.stdin, process.stdout);

rl.question('Enter a password: ', async (password) => {
  await bcryptImpl(password);
  await argon2Impl(password);
  process.exit();
});
